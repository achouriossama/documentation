Pour organizer ton code sous Git avec la mathode Gitflaw:

**feature**  : Branche pour les nouvelles fonctions à developper

**develop**  : Branche qui regroupe tous les developpements

**release**  : Branche pour préparer à la mise en production

**hotfix**   : Branche corriger une bug dans la version en production

**master**   : Branche de production