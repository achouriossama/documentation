python 3.5:

	- ajoute un module typing

python 3.6:

  - F-string
  - **kwargs  assurés d’être ordonnés
  - Ajouter pathlib
  - 1_000_000



python 3.7:

	- La conservation de l’ordre d’insertion dans les dict est désormais officielle.
	- Introduction de la fonction breakpoint avant il faut passer par : import pdb et pdb.set_trace()
        - Ajouter les types élaborés comme Sequence, List, Dict et Any 


python 3.8:

	- print(a := 5) l'opérateur walrus
        - Le / à la fin de la ligne pow(x, y, z=None, /)  ce slash qui indique que les arguments précédant le slash sont des arguments positionnels forcés
        - nouveautés des f-string  f"{x=}, {y=}, {z=}"

python 3.9:

  	- L’opérateur « | » sert donc à merger  deux dict
	- L’opérateur « |= » sert à mettre à jour un dict 
        - « str.removeprefix(substring: string) » permet de supprimer le prefix d’une chaine de caractère
        - « str.removesuffix(substring: string) » quant à elle supprime le suffix
	- list[int] - une liste d'entiers - sans avoir besoin de la bibliothèque de type


python 3.10:

	- Rapports d’erreurs plus précis 
	- Ajouter switch / case
        - Union [X, Y] remplcer par  X | Y 
	- Le concept de TypeAlias
