1) Créer une image avec version

```
docker build -t <nom de l'image:version> .
```

2) créer un tag:

```
docker tag <image_source>:<version>  <images_dest>:<version>
```


3) Connecter au registry de gitlab

docker login registry.gitlab.com

4) Pousser l'image sur Gitlab

docker tag <images_dest>:<version>  registry.gitlab.com/achouriossama/test_python/<images_dest>:<version>


-> Le push / pull <-

* si registry par défaut (docker hub)
```
docker login
```

* si registry particulière
```
docker login <chemin_registry>
```

5) Récupèrer une image à partir d'un registry:
docker pull registry.gitlab.com/achouriossama/test_python/imagetag
* push
```
docker push <registry><nom_image>:<version>
```

* pull
```
docker pull <registry><nom_image>:<version>
```